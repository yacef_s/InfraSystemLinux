# Groupe de yacef_s 975022



D'abord pour gw01 la gateway j'ai configuré les network adapter 1 et 2 pour avoie bridged (automatic) et le host-only pour pouvoir faire
la transition entre les mg01,app01 et cl01  

mg01: host-only
app01: host-only
cl01: host-only

ensuite j'ai installé ma VM GW01 avec 2 carte réseaux bridge et host-only, puis j'ai configuré le fichier /etc/network/interfaces comme ceci ->

```
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug ens33
iface ens33 inet dhcp

# The secondary network interface
auto ens36
iface ens36 inet static
        address 192.168.100.1/24
        dns-nameservers 192.168.100.2
```



Après faire un restart du service networking

#sudo systemctl restart networking

IPtables
Cette étape est à faire sur la VM GW01 j'ai effectué ces commandes bash >

# -A AJOUT DE REGLE a une chaine (-A INPUT)  #
#Conntrack est un composant de Netfilter qui permet de suivre les connexions réseaux dans Linux.#


iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -i ens33 -j ACCEPT   	
iptables -A INPUT -i ens33 -p tcp -s 0/0 -d 0/0 --dport 443 -j ACCEPT 
iptables -A INPUT -i ens33 -p tcp -s 0/0 -d 0/0 --dport 80 -j ACCEPT
iptables -t filter -A INPUT -i ens33 -j REJECT 			# le -t c pour le type la table NAT spécifier la table filter etc...
iptables -t filter -I OUTPUT -o ens33 -j ACCEPT
iptables -A FORWARD -i ens36 -j ACCEPT
iptables -A FORWARD -o ens36 -j ACCEPT
iptables -t nat -A POSTROUTING -o ens33 -j MASQUERADE
sysctl net.ipv4.ip_forward=1
sysctl -q net.ipv4.ip_forward=1

Après avoirs tapé chaque commande bash d'iptables j'enregistre le tous (en sachant que l'ordre des commandes et toujours très importantes)


iptables -save > masav.rules

Le fichier sauvegarde sera créer 
pour faire appel on agis en faisant "iptables-restore < masav.rules" 
Pour garder à chaque reboot la sauvegarde je place masav.rules dans le dossier /etc/
Ensuite je vais dans mon etc/network/interfaces

auto lo
iface lo inet loopback

        pre-up iptables-restore < /etc/masav.rules


Ajout du DNS
éditer le fichier /etc/hostname -> gw01.eftw.local.
éditer le fichier /etc/hosts ->

127.0.0.1       localhost
127.0.1.1       gw01.eftw.local gw01
192.168.100.1   gw01.eftw.local gw01

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters


éditer le fichier /etc/resolv.conf ->

domain eftw.local
search eftw.local
nameserver 192.168.100.3


Configuration MG01
Cette VM MG01 possède une carte réseaux host-only et j'ai configuré le fichier /etc/network/interfaces ->

# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interfac
auto ens33
iface ens33 inet static
        address 192.168.100.3/24
        gateway 192.168.100.1



restart le service networking.
cd /etc
Création du serveur DNS
Editer le fichier /etc/hostname -> mg01.eftw.local
Editer le fichier /etc/hosts ->

127.0.0.1       localhost
127.0.1.1       mg01.eftw.local mg01
192.168.100.3   mg01.eftw.local mg01
# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters


Editer le fichier /etc/resolv.conf ->

domain eftw.local
search eftw.local
nameserver 192.168.100.3


Installer bind9 avec cette commandes -> apt-get install bind9 bind9utils
Editer le fichier /etc/bind/named.conf.local ->

//
// Do any local configuration here
//

// Consider adding the 1918 zones here, if they are not used in your
// organization

//include "/etc/bind/zones.rfc1918";

zone "eftw.local" {
 type master;
 file "/etc/bind/db.eftw.local";
 };
zone "100.168.192.in-addr.arpa" {
 type master;
 file "/etc/bind/db.100.168.192.in-addr.arpa";
 };


Editer le fichier /etc/bind/db.eftw.local ->

$TTL 10800
$ORIGIN eftw.local.
@       IN SOA mg01.eftw.local. root.eftw.local. (
                20160505;
                3h;
                1h;
                1w;
                1h);
@       IN NS   mg01.eftw.local.
mg01    IN A    192.168.100.3
gw01    IN A    192.168.100.1
app01   IN A    192.168.100.2
cl01    IN A    192.168.100.4


Editer le fichier /etc/bind/db.100.168.192.in-addr.arpa ->

$TTL 10800
$ORIGIN 100.168.192.in-addr.arpa.
@       IN SOA mg01.eftw.local. root.eftw.local. (
                20160505;
                3h;
                1h;
                1w;
                1h);
@       IN NS   mg01.eftw.local.
3       IN PTR  mg01.eftw.local.
1       IN PTR  gw01.eftw.local.
2       IN PTR  app01.eftw.local.
4       IN PTR  cl01.eftw.local.


effectuer cette commande bash ->  named-checkconf -z

zone eftw.local/IN: loaded serial 20160505
zone 100.168.192.in-addr.arpa/IN: loaded serial 20160505
zone localhost/IN: loaded serial 2
zone 127.in-addr.arpa/IN: loaded serial 1
zone 0.in-addr.arpa/IN: loaded serial 1
zone 255.in-addr.arpa/IN: loaded serial 1


Editer le fichier //   named.conf.options

acl allowed_clients {
        localhost;
        192.168.100.0/24;
};
options { directory "/var/cache/bind";
        // If there is a firewall between you and nameservers you want to talk to, you may
        // need to fix the firewall to allow multiple ports to talk.  See
        // http://www.kb.cert.org/vuls/id/800113 If your ISP provided one or more IP
        // addresses for stable nameservers, you probably want to use them as forwarders.
        // Uncomment the following block, and insert the addresses replacing the all-0's
        // placeholder.
         forwardrs {
                8.8.8.8;
                8.8.4.4;
         };
        recursion yes;
        allow-query { allowed_clients; };
        forward only;
        //====================================================================== == If BIND
        // logs error messages about the root key being expired, you will need to update
        // your keys.  See https://www.isc.org/bind-keys
        //====================================================================== ==
        dnssec-validation auto;

        listen-on-v6 { any; };
};


restart le service bind9.

sudo named-checkconf
sudo systemctl restart bind9


Mise en place du serveur DHCP
Faire cette commnde bash -> apt install isc-dhcp-server -y
aller dans le fichier cat /etc/default/isc-dhcp-server ->

# Defaults for isc-dhcp-server (sourced by /etc/init.d/isc-dhcp-server)

# Path to dhcpd's config file (default: /etc/dhcp/dhcpd.conf).
#DHCPDv4_CONF=/etc/dhcp/dhcpd.conf
#DHCPDv6_CONF=/etc/dhcp/dhcpd6.conf

# Path to dhcpd's PID file (default: /var/run/dhcpd.pid).
#DHCPDv4_PID=/var/run/dhcpd.pid
#DHCPDv6_PID=/var/run/dhcpd6.pid

# Additional options to start dhcpd with.
#       Don't use options -cf or -pf here; use DHCPD_CONF/ DHCPD_PID instead
#OPTIONS=""

# On what interfaces should the DHCP server (dhcpd) serve DHCP requests?
#       Separate multiple interfaces with spaces, e.g. "eth0 eth1".


E0diter le fichier /etc/dhcp/dhcpd.conf ->

# Définition communes à tous les réseaux supportés
default-lease-time 43200; # 12 heures
max-lease-time 86400; # 24 heures
ddns-update-style none;

# options pour tous les reseaux
option domain-name "eftw.local";
option domain-name-servers 192.168.100.3;
option routers 192.168.100.1;
option ntp-servers 192.168.100.3;

ddns-domainname "eftw.local.";
ddns-rev-domainname "100.168.192.in-addr.arpa.";

# Plage DHCP
subnet 192.168.100.0 netmask 255.255.255.0 {
        range                           192.168.100.2 192.168.100.2;
        range                           192.168.100.4 192.168.100.55;
        option domain-name-servers      192.168.100.3;
        option routers                  192.168.100.1;
        # Réservations DHCP
        host superlinux-eth {
                hardware ethernet 00:0c:29:fd:fa:58;
                fixed-address 192.168.100.2;
        }
}


service isc-dhcp-server restart


ne pas faire sudo apt update



## SSH

### BANNER

nano /etc/issue.net

#################### Authorized access only! ################
# Disconnect immediatly if you are not an authorized user!!!#
########### {ServerName} is monitoring by {login} ###########
######## All actions will be monitored and recorded #########


nano /etc/ssh/sshd_config

Banner /etc/issue.net




ssh-keygen
chmod 400 ~/.ssh/id_rsa


souleyman@app01:~$ sudo ssh-copy-id -i ~/.ssh/id_rsa.pub souleyman@192.168.100.3 -p2222


/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/souleyman/.ssh/id_rsa.pub"


Number of key(s) added: 1

Now try logging into the machine, with:   "ssh -p '2222' 'souleyman@192.168.100.3'"
and check to make sure that only the key(s) you wanted were added.


maintenant je vais authentifier depuis la gateway 

en mettant juste cette ligne de code 

ssh souleyman@192.168.100.3 -p2222
pour se connecter à mg01
où
ssh souleyman@192.168.100.2 -p2223
pour se connecter app01

pour avoir le PAT 

iptables -t nat -A PREROUTING -i ens33 -p tcp --dport 2222 -j DNAT --to-destination 192.168.100.3:22
iptables -t nat -A PREROUTING -i ens33 -p tcp --dport 2223 -j DNAT --to-destination 192.168.100.2:22


#Certificat SSL
On peut générer un certificat autosigné en une ligne de commande : openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -out /path/to/server.crt -keyout /path/to/server.key

Le bout de configuration Apache2 pour le certificat autosigné :

SSLEngine on
SSLCertificateFile /path/to/server.crt
SSLCertificateKeyFile /path/to/server.key
   

# GITLAB

 
sudo apt-get install -y curl openssh-server ca-certificates

curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

sudo EXTERNAL_URL="http://gitlab.eftw.local/" apt-get install gitlab-ee

Il faut maintenant ajouter gitlab.eftw.local dans les /etc/hosts.


# faire du SWAP GIT :

dd if=/dev/zero of=/swapfile bs=1024 count=$((1024 * 1024 * 5))
chown root:root /swapfile
chmod 0600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile none swap sw 0 0" >> /etc/fstab
Je lui met sans pression 5 Go de SWAP.

On installe des dépendances : sudo apt-get install -y curl openssh-server ca-certificates

Go faire du curl2bash : curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

Go installer gitlab : sudo EXTERNAL_URL="http://gitlab.eftw.local/" apt-get install gitlab-ee

Il faut maintenant ajouter gitlab.eftw.local dans les /etc/hosts.

# Etape 8 : Centreon
suivre ce tuto

# Etape 9 : Certificat
On peut générer un certificat autosigné en une ligne de commande : openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -out /path/to/server.crt -keyout /path/to/server.key

Le bout de configuration Apache2 pour le certificat autosigné :

SSLEngine on
SSLCertificateFile /path/to/server.crt
SSLCertificateKeyFile /path/to/server.ke
 
    

